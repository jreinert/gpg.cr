require "./spec_helper"
require "./gpg/*"

private def decrypt(cipher)
  Process.run("gpg", ["-d"]) do |io|
    io.input << cipher
    io.input.close
    io.output.gets_to_end
  end
end

private def encrypt(plain, recipient)
  Process.run("gpg", ["-e", "-r", recipient]) do |io|
    io.input << plain
    io.input.close
    io.output.gets_to_end
  end
end

private def sign(plain)
  Process.run("gpg", ["-s"]) do |io|
    io.input << plain
    io.input.close
    io.output.gets_to_end
  end
end

describe GPG do
  describe ".new" do
    it "succeeds without error" do
      GPG.new
    end
  end

  describe "#list_keys" do
    it "returns a key iterator" do
      build_gpg.list_keys.should be_a(Iterator(GPG::Key))
    end

    it "returns the correct amount of results" do
      build_gpg.list_keys.size.should eq(2)
    end

    it "contains the correct keys" do
      expected_fingerprints = [SECRET_KEY] + PUBLIC_KEYS
      build_gpg.list_keys.map(&.fingerprint).each do |fingerprint|
        expected_fingerprints.includes?(fingerprint).should be_true
      end
    end
  end

  describe "#encrypt" do
    it "produces correct ciphertext" do
      gpg = build_gpg
      secret_key = gpg.list_keys(secret_only: true).first
      cipher = gpg.encrypt("foobar", secret_key(gpg))
      decrypt(cipher).should eq("foobar")
    end
  end

  describe "#decrypt" do
    it "produces correct plaintext" do
      cipher = encrypt("foobar", SECRET_KEY)
      build_gpg.decrypt(cipher).should eq("foobar")
    end
  end

  describe "#sign" do
    it "produces correct signature" do
      signature = sign("foobar")
      build_gpg.sign("foobar").should eq(signature)
    end
  end

  describe "#signers" do
    it "is empty by default" do
      build_gpg.signers.empty?.should be_true
    end

    it "returns a key iterator" do
      build_gpg.signers.should be_a(Iterator(GPG::Key))
    end
  end
end
