require "../spec_helper"

describe GPG::Signers do
  describe "#size" do
    it "is 0 by default" do
      build_gpg.signers.size.should eq(0)
    end
  end

  describe "#<<" do
    it "increases size by 1" do
      gpg = build_gpg
      gpg.signers << secret_key(gpg)
      gpg.signers.size.should eq(1)
    end
  end

  describe "#to_a" do
    it "returns expected keys" do
      gpg = build_gpg
      gpg.signers << secret_key(gpg)
      gpg.signers.to_a.should eq([secret_key(gpg)])
    end
  end
end
