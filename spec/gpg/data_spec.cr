require "../spec_helper"

describe GPG::Data do
  describe ".new" do
    context "without arguments" do
      it "runs without error" do
        GPG::Data.new
      end
    end

    context "with slice as argument" do
      it "runs without error" do
        GPG::Data.new("foobar".to_slice)
      end
    end

    context "with string as argument" do
      it "runs without error" do
        GPG::Data.new("foobar")
      end
    end
  end

  describe "#gets_to_end" do
    it "returns the previously written string" do
      data = GPG::Data.new("foobar")
      data.rewind
      data.gets_to_end.should eq("foobar")
    end
  end

  describe "#<<" do
    it "appends data" do
      data = GPG::Data.new("foobar")
      data << "foo"
      data.rewind
      data.gets_to_end.should eq("foobarfoo")
    end
  end
end
