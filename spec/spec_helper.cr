require "spec"
require "../src/gpg"
require "file_utils"

ENV["GNUPGHOME"] = File.expand_path("../gpg_home", __FILE__)

private def public_key
  `gpg --with-colons --fingerprint test-public@example.com | awk -F: '$1 == "fpr" { print $10; exit }'`.chomp
end

if ENV.has_key?("POPULATE_KEYRING")
  FileUtils.rm_r(ENV["GNUPGHOME"])
  Dir.mkdir_p(ENV["GNUPGHOME"], 0o700)

  system("gpg", %w(--no-tty --batch --gen-key --yes spec/gpg_test_private_key_params))
  system("gpg", %w(--no-tty --batch --gen-key --yes spec/gpg_test_public_key_params))
  system("gpg", %w(--no-tty --batch --yes --delete-secret-keys) + [public_key])
end

PUBLIC_KEYS = [public_key]

SECRET_KEY = `gpg --with-colons --list-secret-keys`.split("\n")[1].split(":")[9]

def build_gpg
  GPG.new.tap { |gpg| gpg.pinentry_mode = LibGPG::PinentryMode::Loopback }
end

def secret_key(gpg)
  gpg.list_keys(secret_only: true).first
end

def public_key(gpg)
  gpg.list_keys(PUBLIC_KEYS.sample).first
end
